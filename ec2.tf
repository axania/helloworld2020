resource "aws_instance" "my_ec2" {
   ami = "ami-0c2f25c1f66a1ff4d"
   instance_type = "t2.micro"
   key_name = "terraform"
   tags {
      Name = "my_ec2"   
   }
}